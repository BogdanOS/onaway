/**
 * Created by Eugene Khrapenko on 19.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */

// import $ from 'jquery';
import SiteInterface from './components/SiteInterface';


document.addEventListener('DOMContentLoaded', () => {
    window.debug = true;
});

window.tablet = false,
window.desktop = false,
window.mobile = false;

window.addEventListener('load', () => {

    
    
    // console.log(window.isIe(), 'is Ie');
    
    if(window.debug){
        console.log('load');
    }
    
    window.siteInterface = new SiteInterface();
});


