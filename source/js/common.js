/**
 * Created by Eugene Khrapenko on 19.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */

window.log = function log () {
    console.log.apply( console, arguments );
};

window.checkBreakpoint = function(){
	let e = document.documentElement,
    	g = document.getElementsByTagName('body')[0],
    	wWidth = window.innerWidth||e.clientWidth||g.clientWidth,
    	wHeight = window.innerHeight||e.clientHeight||g.clientHeight;
	if(wWidth >= 768){
        if(wWidth > 1024){
            desktop = true;
            tablet = false;
            mobile = false;
        } else {
            desktop = false;
            tablet = true;
            mobile = false;
        }
    } else {
		desktop = false;
		tablet = false;
		mobile = true;
	}
};


