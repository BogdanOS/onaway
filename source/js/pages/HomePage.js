import AbstractPage from './AbstractPage';
import { TimelineMax } from 'gsap';


export default class HomePage extends AbstractPage{
    constructor(){
        super();

        this.cssClasses = {
            submitBtn: 'js-submit-form'
            
        };

        this.submtBtn = $(`.${this.cssClasses.submitBtn}`);


        this._signUpClickListener();
        this._init();
    }
    

    _signUpClickListener(){
        this.submtBtn.on('click', (e) => {
            e.preventDefault();
            
            let tl = new TimelineMax();
            // $('.letter-input').css('opacity', '0')

            tl.to($('.letter-input'), 0.3, {alpha: 0, onComplete:() => {
                $('.letter-input').css('display', 'none');
                $('.thenk-message').css('display', 'block');
            }})
            .to($('.thenk-message'), 0.3, {alpha: 1});
            
        });
    }

    _init(){
        super._init();
        console.log('home');
        

    }
}