import { TweenMax } from 'gsap';
import $ from 'jquery';
import slick from '../plugins/slick';


export default class AbstractPage{
    constructor(){
      
        this.cssClasses = {
            burgerBtn: 'js-burger-mobile',
            mobMenu: 'js-header',
            closeNavBtn: 'js-close-nav',
            slickWrapp: 'js-slick'
        };

        desktop = undefined;
        this.burgerBtn = $(`.${this.cssClasses.burgerBtn}`);
        this.mobMenu = $(`.${this.cssClasses.mobMenu}`);
        this.closeNavBtn = $(`.${this.cssClasses.closeNavBtn}`);
        this.slider = $(`.${this.cssClasses.slickWrapp}`);
        if($(window).width() > 1024){
            desktop = true;
        } else {
            this.burgerClickListener();
            this.closeNavClickListener();
            desktop = false;
        }
    }

    sliderSlick(){
        
        

        this.slider.slick({
            arrows: true,
            dots: true,
            infinite: true,
            // adaptiveHeight: true,
            prevArrow: '<div class="slider-btn_prev"><i class="icon icon-slider-prev"></i></div>',
            nextArrow: '<div class="slider-btn_next"><i class="icon icon-slider-next"></i></div>',
            // appendDots: '<div class="slider-dot"><i class="icon icon-slider-dot"></i></div>'
        });
    }


    burgerClickListener(){
        this.burgerBtn.on('click', (e) => {
            e.preventDefault;
            $('body').addClass('menu-open');
            this.mobMenu.css('transform', 'translate(0, 0)');
            this.mobMenu.css('opacity', '1');
        })
    }

    closeNavClickListener(){
        this.closeNavBtn.on('click', (e)=>{
            e.preventDefault;
            $('body').removeClass('menu-open');
            this.mobMenu.css('transform', 'translate(0, -100%)');
            this.mobMenu.css('opacity', '0');
        })
    }

    

    

    _init(){        
       
        if(this.slider.length){
            
            this.sliderSlick();
        }
    }
}