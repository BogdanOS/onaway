import AbstractPage from './AbstractPage';
import $ from 'jquery';
import slick from '../plugins/slick';
import { TimelineMax } from 'gsap';

export default class AboutPage extends AbstractPage{
    constructor(){
        super();

        this.cssClasses = {
            aboutFilter: 'js-about-filter'
        }

        this.aboutFilter = $(`.${this.cssClasses.aboutFilter}`);
        this.filterBtn = this.aboutFilter.find('a');
        this.filterBtnActive = this.aboutFilter.find('a.active');
        this.filterArrow = this.aboutFilter.find('.icon-filter-arrow');

        if($(window).width() > 767){
            desktop = true;
        } else {
            desktop = false;
        }
        this.mobileFilterClick();
        this.mobileFilterActiveChange();
        this._init();
    }


    mobileFilterActiveChange(){
        if(!desktop){
            let firstElem = this.filterBtnActive.closest('li').clone();
            this.filterBtnActive.remove();
            firstElem.prependTo(this.aboutFilter.find('ul'));
        }
       
        
    }

    mobileFilterClick(){
        if(!desktop){
            this.aboutFilter.on('click', () =>{
                let tl = new TimelineMax();
                console.log('click');
                if(!this.aboutFilter.hasClass('filter-open')){
                    
                    let blockH = this.filterBtn.length * 50;
                   
                    tl
                        .to(this.aboutFilter, 0.3, {css: {height: blockH}})
                        .to(this.filterArrow, 0.3, {rotation: 0, onComplete: ()=>{
                            this.aboutFilter.addClass('filter-open');
                        }});
                } else {
                    tl
                        .to(this.aboutFilter, 0.3, {css: {height: 50}})
                        .to(this.filterArrow, 0.3, {rotation: 180, onComplete: ()=>{
                            this.aboutFilter.removeClass('filter-open');
                        }});
                }
            });
        } else{
            return;
        }
    }
    

    _init(){
        super._init();
        
        console.log('about Page');  

    }
}