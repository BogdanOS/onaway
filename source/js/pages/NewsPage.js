import AbstractPage from './AbstractPage';


export default class NewsPage extends AbstractPage{
    constructor(){
        super();


        this.desktop = true;
        this.tablet = false;
        this.mobile = false;

        if ($(window).width() >= 1025) {
            this.desktop = true;
            this.tablet = false;
            this.mobile = false;
        }
        if ($(window).width() >= 768 && $(window).width() <= 1024) {
            this.desktop = false;
            this.tablet = true;
            this.mobile = false;
        } else {
            if ($(window).width() <= 767) {
                this.desktop = false;
                this.tablet = false;
                this.mobile = true;
            }
        }


        this._init();
    }

    equalHeight(group, groupSize) {
        if (!group.length) {
            return;
        }
        groupSize = +(groupSize || 0);
        if (groupSize < 1) {
            groupSize = group.length;
        }
        var start = -groupSize, part;
        while ((part = group.slice(start += groupSize, start + groupSize)) && part.length) {
            part.height(Math.max.apply(null, $.makeArray(part.map(function () {
                return $(this).height();
            }))));
        }
    }

    setHeight(){
        let newGroup = $('.news-i').not('.news-i-big');
        
        if(this.desktop){
            console.log('set');
            this.equalHeight(newGroup, 2);
            this.equalHeight($('.news-web-catalog_item .name'), 4);
        } else if(this.tablet){
            this.equalHeight(newGroup, 2);
            this.equalHeight($('.news-web-catalog_item .name'), 2);
        } else if(this.mobile){
            return;
        }
    }

    _resize(){
        
        $(window).on('resize', ()=>{
            let newGroup = $('.news-i').not('.news-i-big');
            
            if(this.desktop){
                console.log('deskset');
                newGroup.css('height', 'auto');
                $('.news-web-catalog_item .name').css('height', 'auto');
                this.equalHeight(newGroup, 2);
                this.equalHeight($('.news-web-catalog_item .name'), 4);
            } else if(this.tablet){
                newGroup.css('height', 'auto');
                $('.news-web-catalog_item .name').css('height', 'auto');
                this.equalHeight(newGroup, 2);
                this.equalHeight($('.news-web-catalog_item .name'), 2);
            } else if(this.mobile){
                return;
            }
            
        })
    }

    _init(){
        super._init();
        
        this.setHeight();
        this._resize();
        
        console.log('news');
        

    }
}