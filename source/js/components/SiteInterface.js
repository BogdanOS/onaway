import $ from 'jquery';
import HomePage from '../pages/HomePage';
import AboutPage from '../pages/AboutPage';
import ProjectPage from '../pages/ProjectPage';
import CausePage from '../pages/CausePage';
import NewsPage from '../pages/NewsPage';
import ArticlePage from '../pages/ArticlePage';



export default class SiteInterface{
    constructor(){

        this.PAGE_LIST = {
            HOME: 0,
            ABOUT: 1,
            PROJECT: 2,
            CAUSE: 3,
            NEWS: 4,
            ARTICLE: 5
        }

        this._init();
    }
 
    checkPage(){
        let pageId = $('main').data('page-id'),
        pages = this.PAGE_LIST;
        switch(pageId){
            case(pages.HOME):
                window.currentPage = new HomePage();
                break;
            case(pages.ABOUT):
                window.currentPage = new AboutPage();
                break;
            case(pages.PROJECT):
                window.currentPage = new ProjectPage();
                break;
            case(pages.CAUSE):
                window.currentPage = new CausePage();
                break;
            case(pages.NEWS):
                window.currentPage = new NewsPage();
                break;
            case(pages.ARTICLE):
                window.currentPage = new ArticlePage();
                break;    
            
        }
    }

    /**
     * 
     */
    _init(){
        this.checkPage();
    }
}