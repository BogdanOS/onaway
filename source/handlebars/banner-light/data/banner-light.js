bnnerlight:{
    project:{
        first:{
            title: 'DONATE TO onaway',
            text: 'Help us continue our support for the Acate people into the future',
            link: 'donate now'
        },
        second:{
            title: 'OTHER PROJECTS',
            text: 'We work on a variety of other projects supporting indigenous people globally',
            link: 'ALL PROJECTS'
        }
        
    },
    patner:{
        first:{
            title: 'partner with us',
            text: 'If you have a project that you think we could help with please contact us',
            link: 'contact us'
        }
    },
    cause:{
        first:{
            title: 'protectingcivil liberties',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam.',
            link: 'read more'
        },
        second:{
            title: 'Helping to fight rights abuses',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam.',
            link: 'read more'
        }
    },
    causes:{
        first:{
            title: 'onaway’s projects',
            text: 'Nullam rutrum ex nibh, quis ullamcorper ipsum porttitor nec aenean pellentesque mi dui, in mollis.',
            link: 'view projects'
        }
    },
    contact:{
        first:{
            title: 'Stay up to date',
            text: 'Follow Onaway on social media to get recieve the latest news and updates from our projects lorem ipsum dolor sit amet lorem ipsum',
            link: 'Facebook',
            secondlink: 'twitter'
        }
    }
}

