banner:{
    home:{
        img: '../source/images/content/main-banner.png',
        imgmob: '../source/images/home/main-banner-mb.png',
        title: 'Helping tribal peoples to defend their lives and preserve their knowledge',
        link: 'find out more'
    },
    about:{
        img: '../source/images/about/about-banner.png',
        imgmob: '../source/images/about/about-mobile.png',
        title: 'about us',
        text: 'Phasellus quis rhoncus ante, vitae dictum lacus morbi vitae tortor id dolor egestas placerat'
        
    },
    project:{
        title: 'AcatÉ Amazon Conservation',
        subtitle: 'Preserving the Amazon rainforest in partnership with it’s inhabitants and protectors'
    },
    cause:{
        
        title: 'helping the environment',
        subtitle: 'Phasellus quis rhoncus ante, vitae dictum lacus morbi vitae tortor id dolor egestas placerat'
    },
    causes:{
        img: '../source/images/cause/cause-banner.png',
        imgmob: '../source/images/cause/cause-banner-mob.png',
        title: 'causes',
        text: 'Supporting the cultures, languages and lifeways of nature-based tribal peoples'
    },
    projects:{
        img: '../source/images/projects/projects-banner.png',
        imgmob: '../source/images/projects/projects-banner-mob.png',
        title: 'Projects',
        text: 'We provide seed grants to natural peoples projects to help keep ancient knowledge and wisdom alive'
    }
} 